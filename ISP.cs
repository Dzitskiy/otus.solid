﻿namespace SolidNotGood
{
    internal interface IEmpoyee
    {
        void Work();
        void Eat();
    }

    internal class Human : IEmpoyee
    {
        public void Eat()
        {
        }

        public void Work()
        {
        }
    }

    internal class Robot : IEmpoyee
    {
        public void Eat()
        {
        }

        public void Work()
        {
        }
    }
}