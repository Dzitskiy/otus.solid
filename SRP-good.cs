﻿namespace Solid
{
    internal class User
    {
        public bool IsAdmin => true;
    }
    internal class Settings
    {
    }

    class UserAuth
    {
        private User User;
        public UserAuth(User user)
        {
            User = user;
        }
        public bool VerifyCredentials()
        {
            return User.IsAdmin;
        }
    }

    internal class UserSettings
    {
        private User User;
        private UserAuth Auth;

        public UserSettings(User user)
        {
            User = user;
            Auth = new UserAuth(user);
        }

        public void ChangeSettings(Settings settings)
        {
            if (Auth.VerifyCredentials())
            {
                // ...
            }
        }
    }
}